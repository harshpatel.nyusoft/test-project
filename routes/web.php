<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', [App\Http\Controllers\StudentController::class, 'index'])->name('home');
    Route::get('/add-student', [App\Http\Controllers\StudentController::class, 'addStudent'])->name('add-student');
    Route::post('/add-student-submit', [App\Http\Controllers\StudentController::class, 'addStudentSubmit'])->name('add-student-submit');

    Route::post('/import-student', [App\Http\Controllers\StudentController::class, 'importStudentSubmit'])->name('import-student');

    Route::get('/view-student/{id}', [App\Http\Controllers\StudentController::class, 'viewStudent'])->name('view-student');

    Route::get('/edit-student/{id}', [App\Http\Controllers\StudentController::class, 'editStudent'])->name('edit-student');
    Route::post('/edit-student-submit', [App\Http\Controllers\StudentController::class, 'editStudentSubmit'])->name('edit-student-submit');

    Route::get('/delete-student-submit', [App\Http\Controllers\StudentController::class, 'deleteStudent'])->name('delete-student-submit');

});

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Update Student') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('edit-student-submit') }}" id="form-student" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" name="id" value="{{ $Students->id }}">

                        <div class="row mb-3">
                            <label for="first_name" class="col-md-4 col-form-label text-md-end">{{ __('First Name') }}</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ $Students->first_name }}" required autocomplete="first_name" autofocus>

                                @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                       <div class="row mb-3">
                            <label for="last_name" class="col-md-4 col-form-label text-md-end">{{ __('Last Name') }}</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ $Students->last_name }}" required autocomplete="last_name" autofocus>

                                @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="age" class="col-md-4 col-form-label text-md-end">{{ __('Age') }}</label>

                            <div class="col-md-6">
                                <input id="age" type="number" class="form-control @error('age') is-invalid @enderror" name="age" value="{{ $Students->age }}" required autocomplete="age">

                                @error('age')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="subjects" class="col-md-4 col-form-label text-md-end">{{ __('Subjects') }}</label>

                            <div class="col-md-6">
                                <input type="checkbox" id="subjects" name="subjects[]" value="Maths" @if(in_array('Maths', explode(',', $Students->subjects))) checked @endif>
                                <label for="subjects">Maths</label><br>
                                <input type="checkbox" id="subjects" name="subjects[]" value="History" @if(in_array('History', explode(',', $Students->subjects))) checked @endif>
                                <label for="subjects"> History</label><br>
                                <input type="checkbox" id="subjects" name="subjects[]" value="Computer Science" @if(in_array('Computer Science', explode(',', $Students->subjects))) checked @endif>
                                <label for="subjects"> Computer Science</label><br><br>
                                @error('subjects')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="grade" class="col-md-4 col-form-label text-md-end">{{ __('Grade') }}</label>

                            <div class="col-md-6">
                                <input id="grade" type="text" class="form-control @error('grade') is-invalid @enderror" name="grade" value="{{ $Students->grade }}" required autocomplete="grade">

                                @error('grade')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="average_score" class="col-md-4 col-form-label text-md-end">{{ __('Average Score') }}</label>

                            <div class="col-md-6">
                                <input id="average_score" type="text" class="form-control @error('average_score') is-invalid @enderror" name="average_score" value="{{ $Students->average_score }}" required autocomplete="average_score">

                                @error('average_score')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="average_score" class="col-md-4 col-form-label text-md-end">{{ __('Image') }}</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" autocomplete="image">

                                @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                                &nbsp;&nbsp;
                                <a href="{{ route('home') }}" class="btn btn-warning">
                                    {{ __('Back') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){

        $('#form-student').validate({
            // debug:true,
            ignore: [],
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                age: {
                    required: true,
                },
                grade: {
                    required: true,
                },
                average_score: {
                    required: true,
                },
            },
            messages: {
                first_name: {
                    required: "Please enter First name",
                },
                last_name: {
                    required: "Please enter Last Name",
                },
                age: {
                    required: "Please enter Age",
                },
                grade: {
                    required: "Please enter Grade",
                },
                average_score: {
                    required: "Please enter Average Score",
                }
                
            },
            errorPlacement: function(error, element) {  
                error.insertAfter(element);                           
             },
            highlight: function (element) {
                $(element).addClass('error');
             },
            unhighlight: function (element) {
                $(element).removeClass('error')
            },
            submitHandler: function (form) {
                form.submit();
            }

        });
    });
</script>
@endsection

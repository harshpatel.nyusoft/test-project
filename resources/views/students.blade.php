@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Students List') }}
                    <a href="{{ route('add-student') }}" class="btn btn-primary" style="float:right;">Add Student</a>&nbsp;
                    <a href="javascript:void(0)" id="import" class="btn btn-primary mr-2" style="float:right;">Import</a>&nbsp;
                </div>
                <form action="{{route('import-student')}}" method="POST" id="form-import" enctype="multipart/form-data">
                @csrf
                <input type="file" style="display:none;" name="file" id="file">
                </form>

                <div class="card-body">
                    @if(Session::has('message'))
                        <p class="mt-10 alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-12 table-responsive">
                            <table class="table table-bordered student_datatable">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Image</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Age</th>
                                        <th>Subjects</th>
                                        <th>Grade</th>
                                        <th>average_score</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Student</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close" data-bs-dismiss="modal"><span aria-hidden="true">×</span></button>
                </div>
                <form method="GET" action="{{ route('delete-student-submit') }}">
                
                    <div class="modal-body">                
                       <span id="deleteMessage"></span>
                            <input type="hidden" name="delete_id" id="delete_id">
                      </div>   
                    <div class="modal-footer">
                        <button class="btn btn-light" type="button" id="cancel" data-bs-dismiss="modal">Close</button>
                        <button type="submit" data-dismiss="modal" id="delete" class="btn btn-primary" >Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function deleteStudent(id){
        $('#deleteMessage').html("Are you sure you want to delete Student ?"); 
        $('#delete').html("Delete"); 
        $('#cancel').css("display","block"); 
        $('#deleteModal').modal('show');
        $('#delete_id').val(id);
    }

  $(function () {

    $('#delete').on('click', function(e) {
        $('#deleteModal').modal('hide');
    });
    var table = $('.student_datatable').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('home') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'image', name: 'image'},
            {data: 'first_name', name: 'first_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'age', name: 'age'},
            {data: 'subjects', name: 'subjects'},
            {data: 'grade', name: 'grade'},
            {data: 'average_score', name: 'average_score'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });


    $("#import").click(function(){ 
        $("#file").trigger('click');
    });

    $("#file").on('change',function(){ 
        $("#form-import").submit();
    });



  });
</script>
@endsection

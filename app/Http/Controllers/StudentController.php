<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Students;
use DataTables;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Validator;
use File;

class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    // home student list function start

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Students::select('id', 'first_name', 'last_name', 'age', 'subjects', 'grade', 'average_score', 'image')->get();
            return Datatables::of($data)->addIndexColumn()
                ->editColumn('image', function($row){
                    $btn = '<img src="uploads/images/'.$row->image.'" width=50px>';
                    return $btn;
                })
                ->addColumn('action', function($row){
                    $btn = '<a href="'.route('edit-student', $row->id).'"  class="btn btn-primary btn-sm">Edit</a>&nbsp;';
                    $btn .= '<a href="javascript:void(0)" onclick="deleteStudent('.$row->id.');" class="btn btn-danger btn-sm">Delete</a>';
                    return $btn;
                })
                ->rawColumns(['action', 'image'])
                ->make(true);
        }

        return view('students');
    }

    // home student list function end



    //student add form function

    public function addStudent()
    {
        return view('add-student');
    }

    //student add form function end




    //student add form submit function

    public function addStudentSubmit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required',
            'subjects' => 'required',
            'grade' => 'required',
            'average_score' => 'required',
          ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        } else {

            $extention = $request->file('image')->getClientOriginalExtension();
            $filename1 = rand(111,999).time() . '.' . $extention;
            $request->file('image')->move('uploads/images/', $filename1);

            $subjects = implode(',', $request->subjects);
            // echo $subjects;die;
            $Students = new Students;
            $Students->image = $filename1;
            $Students->first_name = $request->first_name;
            $Students->last_name = $request->last_name;
            $Students->age = $request->age;
            $Students->subjects = $subjects;
            $Students->grade = $request->grade;
            $Students->average_score = $request->average_score;
            $Students->save();

            \Session::flash('message', 'Student created successfully.'); 
            \Session::flash('alert-class', 'alert-success'); 
            return redirect()->route('home');
        }
    }

    //student add form submit function end




    //student edit form function

    public function editStudent($id)
    {
        $Students = Students::where('id', $id)->first();
        return view('edit-student', ['Students' => $Students]);
    }

    //student edit form function end




    //student edit form submit function

    public function editStudentSubmit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'age' => 'required',
            'subjects' => 'required',
            'grade' => 'required',
            'average_score' => 'required',
          ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withErrors($validator)
                            ->withInput();
        } else {

            $subjects = implode(',', $request->subjects);
            // echo $subjects;die;
            $Students = Students::where('id', $request->id)->first();
            if($request->file('image')){
                $extention = $request->file('image')->getClientOriginalExtension();
                $filename1 = rand(111,999).time() . '.' . $extention;
                $request->file('image')->move('uploads/images/', $filename1);
    
                $Students->image = $filename1;
            }
            $Students->first_name = $request->first_name;
            $Students->last_name = $request->last_name;
            $Students->age = $request->age;
            $Students->subjects = $subjects;
            $Students->grade = $request->grade;
            $Students->average_score = $request->average_score;
            $Students->save();

            \Session::flash('message', 'Student updated successfully.'); 
            \Session::flash('alert-class', 'alert-success'); 
            return redirect()->route('home');
        }
    }

    //student edit form submit function end



    //student delete function

    public function deleteStudent(Request $request)
    {
        Students::where('id', $request->delete_id)->delete();
        \Session::flash('message', 'Student deleted successfully.'); 
        \Session::flash('alert-class', 'alert-success'); 

        return redirect()->route('home');
    }

    //student delete function end




    //student data import function

    public function importStudentSubmit(Request $request)
    {       

        $file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
          
        if(isset($_FILES['file']['name']) && in_array($_FILES['file']['type'], $file_mimes)) {
          
            $arr_file = explode('.', $_FILES['file']['name']);
            $extension = end($arr_file);
          
            if('csv' == $extension) {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
            } else {
                $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
            }
      
            $spreadsheet = $reader->load($_FILES['file']['tmp_name']);
      
            $sheetData = $spreadsheet->getActiveSheet()->toArray();
      
            if (!empty($sheetData)) {
                for ($i=1; $i<count($sheetData); $i++) { //skipping first row
                    $name = $sheetData[$i][0];
                    $email = $sheetData[$i][1];
                    $company = $sheetData[$i][2];

                    $Students = new Students;
                    $Students->first_name = $sheetData[$i][1];
                    $Students->last_name = $sheetData[$i][2];
                    $Students->age = $sheetData[$i][3];
                    $Students->subjects = $sheetData[$i][4];
                    $Students->grade = $sheetData[$i][5];
                    $Students->average_score = $sheetData[$i][6];
                    $Students->image = $sheetData[$i][7];
                    $Students->save();
     
                }
            }

            \Session::flash('message', 'Records inserted successfully'); 
            \Session::flash('alert-class', 'alert-success'); 
            return redirect()->route('home');
        } else {
            \Session::flash('message', 'Upload only CSV or Excel file.'); 
            \Session::flash('alert-class', 'alert-danger'); 
            return redirect()->route('home');
        }
    }

    //student data import function end

}
